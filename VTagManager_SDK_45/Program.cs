﻿using log4net;
using log4net.Config;
using System;
using System.Reflection;
using vtagmanager;
using vtagmanager.libs.messages;

namespace VTagManager_SDK_45
{
    class Program
    {        
        static void Main(string[] args)
        {
            VTagProgram program = new VTagProgram();
            //VTagGPSProgram program = new VTagGPSProgram();

        }

        public class VTagProgram
        {
            VTagManager manager;
            int connectedCount = 0;
            private readonly ILog log;
            public VTagProgram()
            {
                var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
                XmlConfigurator.Configure(logRepository, new System.IO.FileInfo("VTagManager_SDK_45.exe.config"));
                log = LogManager.GetLogger(typeof(Program));

                log.Info("Starting App...");

                this.manager = VTagManager.Instance;
                manager.ConnectionStatusEvent += Manager_ConnectionStatusEvent;
                manager.NewAlarmIndicationEvent += Manager_NewAlarmIndicationEvent;
                manager.NewCommandResponseEvent += Manager_NewCommandResponseEvent;
                manager.NewSecurityIndicationEvent += Manager_NewSecurityIndicationEvent;
                manager.NewSensorReadingEvent += Manager_NewSensorReadingEvent;
                manager.NewSensorStatisticEvent += Manager_NewSensorStatisticEvent;
                manager.NewTagEvent += Manager_NewTagEvent;

                manager.AddGateway(VTagType.VTag, "192.168.1.169", 8422, "admin", "change", "main_gateway", "infinid", false);
                manager.AddGateway(VTagType.VTagGPS, "192.168.1.19", 8422, "admin", "change", "main_gps_gateway", "gps_infinid", false);

                System.Console.ReadLine();

                manager.Dispose();

                manager.ConnectionStatusEvent -= Manager_ConnectionStatusEvent;
                manager.NewAlarmIndicationEvent -= Manager_NewAlarmIndicationEvent;
                manager.NewCommandResponseEvent -= Manager_NewCommandResponseEvent;
                manager.NewSecurityIndicationEvent -= Manager_NewSecurityIndicationEvent;
                manager.NewSensorReadingEvent -= Manager_NewSensorReadingEvent;
                manager.NewSensorStatisticEvent -= Manager_NewSensorStatisticEvent;
                manager.NewTagEvent -= Manager_NewTagEvent;
            }

            private void Manager_NewSecurityIndicationEvent(SecurityIndication securityIndication)
            {
                log.Info(string.Format("********Got New Security Ind:{0}", securityIndication.ToString()));
            }

            private void Manager_NewTagEvent(TagObservation readTag)
            {
                log.Info(string.Format("********Got New Tag Event:{0}", readTag.ToString()));
            }

            private void Manager_NewSensorStatisticEvent(SensorStatistic sensorStatistic)
            {
                log.Info(string.Format("********Got New SensorStatistic Event:{0}", sensorStatistic.ToString()));
            }

            private void Manager_NewSensorReadingEvent(SensorReading sensorReading)
            {
                log.Info(string.Format("********Got New SensorReading Event:{0}", sensorReading.ToString()));
            }

            private void Manager_NewCommandResponseEvent(QueuedCommand queuedCommand)
            {
                log.Info(string.Format("********Got New CommandResponse Event:{0}", queuedCommand.PrintOut()));
            }

            private void Manager_NewAlarmIndicationEvent(AlarmIndication alarmIndication)
            {
                log.Info(string.Format("********Got New Alarm Event:{0}", alarmIndication.ToString()));
            }

            private void Manager_ConnectionStatusEvent(ConnectionStatus connectionStatus)
            {
                if (connectionStatus.IsConnected)
                    connectedCount++;
                else
                    connectedCount--;

                log.Info(string.Format("********Got New ConnectionStatus Event:{0}", connectionStatus.ToString()));

                if (connectionStatus.IsConnected)
                {
                    try
                    {
                        //First plugin each Fixed tag and then run Set Position on each Fixed tags:
                        //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.SetPosition("001D76", 337.93, -711.94, 1, "infinid");

                        //Next, plug in the asset tags and they will discover their nearest fixed neighbors

                        //Finally, experiment with testing our various commands. Please refer to the SDK Documentation for details.

                        //-------TAG COMMANDS ---------
                        //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.GetPosition("005B21", "infinid");
                        QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.Buzz("004000", "gps_infinid");
                        //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.Activate("005B21", "infinid");
                        //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.SetThreshold("005B21", ThresholdType.Acceleration_Upper_g, 1, 100, "infinid");
                        //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.GetThreshold("005B21", ThresholdType.Acceleration_Upper_g, "infinid");
                        //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.ResetTag("005B21", "infinid");
                        //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.SetDwellTime("005B21",0, "infinid");
                        //-------GATEWAY COMMANDS--------
                        //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.SetGPSDiscoveryMode("main_gps_gateway", GPSDiscoveryMode.ImmediatelyUponMovement);
                        //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.ResetNetwork("main_gateway");
                        //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.SetReportingInterval("main_gateway", ReportingInterval.ThirtyMinutes);
                        //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.SetSecurity("main_gateway", SecurityMode.on, -50);
                        bool ranCommand = manager.RunCommand(queuedCommand);
                        log.Info("Running command:" + queuedCommand.ToString());
                        //Can also cancel commands
                        //manager.CancelCommand(queuedCommand.getId());
                    }
                    catch (Exception exc)
                    {
                        log.Error(string.Format("********************************************Got exception:{0}", exc.Message));
                    }
                }
            }
        }

        public class VTagGPSProgram
        {
            VTagManager manager;
            int connectedCount = 0;
            private readonly ILog log;
            public VTagGPSProgram()
            {
                var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
                XmlConfigurator.Configure(logRepository, new System.IO.FileInfo("VTagManager_SDK_45.exe.config"));
                log = LogManager.GetLogger(typeof(Program));

                log.Info("Starting App...");

                this.manager = VTagManager.Instance;
                manager.ConnectionStatusEvent += Manager_ConnectionStatusEvent;
                manager.NewAlarmIndicationEvent += Manager_NewAlarmIndicationEvent;
                manager.NewCommandResponseEvent += Manager_NewCommandResponseEvent;
                manager.NewSensorReadingEvent += Manager_NewSensorReadingEvent;
                manager.NewSensorStatisticEvent += Manager_NewSensorStatisticEvent;
                manager.NewTagEvent += Manager_NewTagEvent;

                manager.AddGateway(VTagType.VTag, "192.168.1.169", 8422, "admin", "change", "main_gateway", "infinid", false);

                System.Console.ReadLine();

                manager.Dispose();

                manager.ConnectionStatusEvent -= Manager_ConnectionStatusEvent;
                manager.NewAlarmIndicationEvent -= Manager_NewAlarmIndicationEvent;
                manager.NewCommandResponseEvent -= Manager_NewCommandResponseEvent;
                manager.NewSensorReadingEvent -= Manager_NewSensorReadingEvent;
                manager.NewSensorStatisticEvent -= Manager_NewSensorStatisticEvent;
                manager.NewTagEvent -= Manager_NewTagEvent;
            }

            private void Manager_NewTagEvent(TagObservation readTag)
            {
                log.Info(string.Format("********Got New Tag Event:{0}", readTag.ToString()));
            }

            private void Manager_NewSensorStatisticEvent(SensorStatistic sensorStatistic)
            {
                log.Info(string.Format("********Got New SensorStatistic Event:{0}", sensorStatistic.ToString()));
            }

            private void Manager_NewSensorReadingEvent(SensorReading sensorReading)
            {
                log.Info(string.Format("********Got New SensorReading Event:{0}", sensorReading.ToString()));
            }

            private void Manager_NewCommandResponseEvent(QueuedCommand queuedCommand)
            {
                log.Info(string.Format("********Got New CommandResponse Event:{0}", queuedCommand.PrintOut()));
            }

            private void Manager_NewAlarmIndicationEvent(AlarmIndication alarmIndication)
            {
                log.Info(string.Format("********Got New Alarm Event:{0}", alarmIndication.ToString()));
            }

            private void Manager_ConnectionStatusEvent(ConnectionStatus connectionStatus)
            {
                if (connectionStatus.IsConnected)
                    connectedCount++;
                else
                    connectedCount--;

                log.Info(string.Format("********Got New ConnectionStatus Event:{0}", connectionStatus.ToString()));

                if (connectionStatus.IsConnected)
                {
                    try
                    {
                        //-------TAG COMMANDS--------
                        //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.GetPosition("005B21", "infinid");
                        //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.AccelSensor("001BC7", false, "infinid");
                        //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.Activate("001BC7", "infinid");
                        //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.SetThreshold("001BC7", ThresholdType.Acceleration_Upper_g, 1, 100, "infinid");
                        //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.GetThreshold("001BC7", ThresholdType.Acceleration_Upper_g, "infinid");
                        //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.ResetTag("001BC7", "infinid");
                        //-------GATEWAY COMMANDS--------
                        //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.ResetNetwork("main_gateway");
                        //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.SetReportingInterval("main_gateway", ReportingInterval.ThirtyMinutes);
                        //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.SetSecurity("main_gateway", SecurityMode.on, -50);
                        QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.SetGPSDiscoveryMode("main_gateway", GPSDiscoveryMode.After5MinutesInactivity);
                        manager.RunCommand(queuedCommand);
                        //Can also cancel commands
                        //manager.CancelCommand(queuedCommand.getId());
                    }
                    catch (Exception exc)
                    {
                        log.Error(string.Format("********************************************Got exception:{0}", exc.Message));
                    }
                }
            }
        }

    }
}
